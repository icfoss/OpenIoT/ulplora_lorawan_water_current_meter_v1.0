## WATER CURRENT METER
*** 

The LoRaWAN Water Current Meter is a device designed for monitoring water velocity in various environments, providing valuable data for applications such as hydrology, environmental monitoring, and water resource management. This project utilizes LoRaWAN technology for wireless communication, allowing remote and energy-efficient data transmission.

![alt text](https://gitlab.com/icfoss1675233/ulplora_lorawan_water_current_meter_v1.0/-/raw/master/Images/wading_rod_.JPG)

 # Prerequisites

- Arduino IDE
- ULPLoRa (https://gitlab.com/icfoss/OpenIoT/ulplora)
- Savenious rotor type sensor

# Getting Started

[How to code](https://gitlab.com/icfoss/OpenIoT/ulplora_lorawan_water_current_meter_v1.0/-/blob/master/Firmware/water_sensor_sleep_final/setup.md?ref_type=heads)

# Contributing

Instructions coming up soon.

# License

This project is licensed under the MIT License - see the LICENSE.md file for details

# Acknowledgments

- [Abdul Arshad V S](https://gitlab.com/abdularshadvs)
- [Jaison Jacob](https://gitlab.com/jaison_j)

# Changelog
