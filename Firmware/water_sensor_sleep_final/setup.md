## Water Current Meter_v1.0
<br>

### Prerequisites
 -----------------
  - [Arduino IDE v1.8.19](https://gitlab.com/icfoss1675233/ulplora_lorawan_water_current_meter_v1.0/-/raw/master/Hardware/wading_rod_wiring_diagram.jpg)
  - [MCCI lorawan lmic library v4.0.0](https://github.com/mcci-catena/arduino-lmic)
  - [ULP LoRa](https://gitlab.com/icfoss/OpenIoT/ulplora)
  - [Wading rod sensor](https://emconindia.com/sensorsmo)
  - [Battery](https://robu.in/product/orange-ifr32650-6000mah-lifepo4-battery/)

### Getting Started 
----
- Connect the components as shown in the ***Wiring diagram*** [here](https://gitlab.com/icfoss1675233/ulplora_lorawan_water_current_meter_v1.0/-/raw/master/Hardware/wading_rod_wiring_diagram.jpg)
- Install ***Arduino IDE***.[here](https://www.arduino.cc/en/Guide/)
- Install ***MCCI lorawan lmic library*** [here](https://docs.arduino.cc/software/ide-v1/tutorials/installing-libraries)
- config the library using ***lmic_project_config.h*** inside the ***project_config*** folder.[here](https://github.com/mcci-catena/arduino-lmic#configuration)
- Select Board : ***Arduino Pro or Pro Mini***
* **Configuration** 

    ```sh
    # Set Transmission interval
    const unsigned long TX_INTERVAL = 900; 
    
    # Set Network session key in Hex Array Format
    static const PROGMEM u1_t NWKSKEY[16] = { 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00 };

    # Set App session key in Hex Array Format
    static const u1_t PROGMEM APPSKEY[16] = { 0x00, 0x0,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00 };

    # Set Device address
    static const u4_t DEVADDR = 0x00000000 ;    
    ```
- Upload the program





